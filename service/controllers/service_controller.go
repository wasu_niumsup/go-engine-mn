package controllers

import (
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/pdk"
	"techberry-go/micronode/service/accessors"
    "techberry-go/micronode/service/commons"
)

type ServiceController struct {
	TraceId         string
	Logger          facade.LogEvent
	Context         pdk.Context
	Connector       pdk.Connector
	ServiceNode     pdk.ServiceNode
	Config          facade.YamlParser
	Handler         facade.Handler
	Version         string
	ServiceAccessor *accessors.ServiceAccessor
    ServiceCommon *commons.ServiceCommon
}
