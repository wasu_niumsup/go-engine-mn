package accessors

import (
	"techberry-go/common/v2/facade"
	"techberry-go/common/v2/pdk"
    "techberry-go/micronode/service/commons"
)

type ServiceAccessor struct {
	TraceId     string
	Logger      facade.LogEvent
	Context     pdk.Context
	Connector   pdk.Connector
	ServiceNode pdk.ServiceNode
	Config      facade.YamlParser
	Handler     facade.Handler
	Version     string
    ServiceCommon *commons.ServiceCommon
}
