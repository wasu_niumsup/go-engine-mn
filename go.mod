module techberry-go/micronode

go 1.15

require techberry-go/common/v2 v2.1.0

replace techberry-go/common/v2 => ../../techberry-go/common
